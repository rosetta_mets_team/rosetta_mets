from setuptools import setup

VERSION='0.1~git'

setup (
    name='rosetta_mets',
    version=VERSION,
    author='Sean Mosely',
    author_email='sean.mosely@gmail.com',
    description='Tool for creating METS XML files for Rosetta SIPs',
    keywords='Ex Libris Rosetta METS XML SIP',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.6',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4',
        'Natural Language :: English'
    ],
    download_url = 'https://bitbucket.org/johnny_falvo/rosetta_mets',
    package_dir={'':'src'},
    py_modules=['mets',],
    install_requires=('lxml'),
    entry_points='''
    [console_scripts]
    mets = mets:main
    '''
    )
