from lxml import etree as ET
import sys
# import mimetypes
import hashlib
import os
import time
import argparse
import datetime
from collections import OrderedDict
# import xml.ET.ElementTree as ET 
# this doesn't work, but it would be good to refactor the code so it does!

__version__ = '0.1'

# declare namespaces
DNX_NS = "http://www.exlibrisgroup.com/dps/dnx"
METS_NS = "http://www.loc.gov/METS/"
DC_NS = "http://purl.org/dc/elements/1.1/"
DCTERMS_NS = "http://purl.org/dc/terms/"
XSI_NS = "http://www.w3.org/2001/XMLSchema-instance"
XLIN_NS = "http://www.w3.org/1999/xlink"

mets_nsmap = {
    'mets': METS_NS,
    }
dnx_nsmap = {
    None: DNX_NS
}
dc_nsmap = {
    "dc": DC_NS,
    "dcterms": DCTERMS_NS,
    "xsi": XSI_NS,
}
xlin_nsmap = {
    'xlin': XLIN_NS,
}

class Mets:
    def __init__(self):
        self.root = ET.Element("{%s}mets" % (METS_NS,), nsmap=mets_nsmap)

    def add_file_level_event(self, filename, event_list):
        """For adding a file-level event to a constructed mets object, based on
           the file's name."""
        amdsec_list = self.root.xpath("//mets:amdSec", namespaces={"mets": "http://www.loc.gov/METS/"})
        for amdsec in amdsec_list:
            if amdsec.xpath("./*/*/*/*/*/*/key[@id='fileOriginalPath']", namespaces={"mets": "http://www.loc.gov/METS/"}):
                if amdsec.xpath("./*/*/*/*/*/*/key[@id='fileOriginalPath']", namespaces={"mets": "http://www.loc.gov/METS/"})[0].text == filename:
                    dnx = amdsec.xpath("./mets:digiprovMD/mets:mdWrap/mets:xmlData/dnx", namespaces={"mets": "http://www.loc.gov/METS/"})[0]
                    section = ET.SubElement(dnx, "section", id="event")
                    record = ET.SubElement(section, "record")
                    for event_id in event_list.keys():
                        key = ET.SubElement(record, "key", id=event_id)
                        key.text = event_list[event_id]

    def print_mets(self, pretty_print=True, utf8=False):
        """prints the mets object to stdout."""
        if utf8 == True:
            print(ET.tostring(self.root, pretty_print=pretty_print, encoding='utf-8'))
        else:
            print(ET.tostring(self.root, pretty_print=pretty_print))

    def write_to_file(self, filepath, pretty_print=True, utf8=False):
        """Writes the mets object to a file location of your choice."""
        if utf8 == True:
            with open(filepath, 'wb') as xml:
                xml.write(ET.tostring(self.root, pretty_print=pretty_print, encoding='utf-8'))
                xml.close()
        else:
            with open(filepath, 'wb') as xml:
                xml.write(ET.tostring(self.root, pretty_print=pretty_print))
                xml.close()            

    def as_string(self, pretty_print=True, utf8=False):
        """returns the mets object as a string."""
        if utf8 == True:
            return(ET.tostring(self.root, pretty_print=pretty_print, encoding='utf-8'))
        else:
            return(ET.tostring(self.root, pretty_print=pretty_print))


# Generic parent classes

class Amd(object):
    def __init__(self):
        self.root = ET.Element("{%s}amdSec" % (METS_NS,), nsmap=mets_nsmap)


class MdExt(object):
    def __init__(self, md_type=None, **kwargs):
        self.root = ET.Element("{%s}%s" % (METS_NS, md_type), nsmap=mets_nsmap)
        self.mdWrap = ET.SubElement(self.root,
            "{%s}mdWrap" % METS_NS, MDTYPE="OTHER", OTHERMDTYPE="dnx")
        self.xmlData = ET.SubElement(self.mdWrap,"{%s}xmlData" % (METS_NS,) )
        self.dnx = ET.SubElement(self.xmlData, "dnx", nsmap=dnx_nsmap)
        for name, dict_list in kwargs.items():
            if dict_list != None:
                section = ET.SubElement(self.dnx, "section", id=name)
                for entry in dict_list:
                    record = ET.SubElement(section, "record")
                    for key, value in entry.items():
                        k = ET.SubElement(record, "key", id=key)
                        k.text = value

class TechMd(MdExt):
    def __init__(self, **kwargs):
        super(TechMd, self).__init__(md_type="techMD", **kwargs)

class RightsMd(MdExt):
    def __init__(self, **kwargs):
        super(RightsMd, self).__init__(md_type="rightsMD", **kwargs)

class SourceMd(MdExt):
    def __init__(self, **kwargs):
        super(SourceMd, self).__init__(md_type="sourceMD", **kwargs)

class DigiprovMd(MdExt):
    def __init__(self, **kwargs):
        super(DigiprovMd, self).__init__(md_type="digiprovMD", **kwargs)


# IE level classes

class IeDmd:
    """For building the ie-dmd section of the METS and populating it
       with the relevant Dublin Core metadata for the IE"""
    def __init__(self, ie_dmd_dict):
        self.root = ET.Element("{%s}dmdSec" % (METS_NS,), nsmap=mets_nsmap,
            ID="ie-dmd")
        self.mdWrap = ET.SubElement(self.root, "{%s}mdWrap" % (METS_NS,),
            MDTYPE="DC")
        self.xmlData = ET.SubElement(self.mdWrap, "{%s}xmlData" % (METS_NS,))
        self.record = ET.SubElement(self.xmlData, "{%s}record" % (DC_NS,),
            nsmap=dc_nsmap)
        for key in ie_dmd_dict.keys():
            tag = key
            if " " in tag:
                tag_list = tag.split(" ")
                tag = tag_list[0]
                attribute = tag_list[1]
            else:
                attribute = None
            tag_components = tag.split(":")
            tag_prefix = tag_components[0]
            tag_name = tag_components[1]
            if attribute:
                # split up the attribute from its value
                attribute_details = attribute.split("=")
                attribute = attribute_details[0]
                attribute_value = attribute_details[1]
                if ":" in attribute:
                    attribute_components = attribute.split(":")
                    attribute_prefix = attribute_components[0]
                    attribute_name = attribute_components[1]
                else:
                    attribute_name = attribute
                    attribute_prefix = None
            # first, most complex circumstance:
            # does it have a namespaced attribute?
            if attribute and attribute_prefix:
                if tag_prefix == "dc":
                    element = ET.Element("{%s}%s" % (DC_NS, tag_name))
                elif tag_prefix == "dcterms":
                    element = ET.Element("{%s}%s" % (DCTERMS_NS, tag_name))
                if attribute_prefix == "xsi":
                    element.set("{%s}%s" % (XSI_NS, attribute_name), attribute_value)
                element.text = ie_dmd_dict[key]
                self.record.append(element)
            # if not, does it have an un-namespaced attribute?
            elif attribute and attribute_name:
                if tag_prefix == "dc":
                    element = ET.Element("{%s}%s" % (DC_NS, tag_name))
                elif tag_prefix == "dcterms":
                    element = ET.Element("{%s}%s" % (DCTERMS_NS, tag_name))
                element.set("%s" % (attribute_name))
                element.text = ie_dmd_dict[key]
                self.record.append(element)
            else:
                if tag_prefix == "dc":
                    element = ET.Element("{%s}%s" % (DC_NS, tag_name))
                elif tag_prefix == "dcterms":
                    element = ET.Element("{%s}%s" % (DCTERMS_NS, tag_name))
                element.text = ie_dmd_dict[key]
                self.record.append(element)


class IeAmd(Amd):
    def __init__(self, webHarvesting=None, generalIECharacteristics=None, objectIdentifier=None,
                 cms=None, accessRightsPolicy=None, eventList=None):
        super(IeAmd, self).__init__()
        self.root.attrib['ID'] = 'ie-amd'
        self.root.append(IeAmdTech(
            webHarvesting=webHarvesting,
            generalIECharacteristics=generalIECharacteristics,
            objectIdentifier=objectIdentifier, CMS=cms).root)
        self.root.append(IeAmdRights(
            accessRightsPolicy=accessRightsPolicy).root)
        self.root.append(IeAmdSource().root)
        self.root.append(IeAmdDigiprov(event=eventList).root)

class IeAmdTech(TechMd):
    def __init__(self, **kwargs):
        super(IeAmdTech, self).__init__(**kwargs)
        self.root.attrib['ID'] = 'ie-amd-tech'

class IeAmdRights(RightsMd):
    def __init__(self, **kwargs):
        super(IeAmdRights, self).__init__(**kwargs)
        self.root.attrib['ID'] = 'ie-amd-rights'

class IeAmdSource(SourceMd):
    def __init__(self, **kwargs):
        super(IeAmdSource, self).__init__(**kwargs)
        self.root.attrib['ID'] = 'ie-amd-source'

class IeAmdDigiprov(DigiprovMd):
    def __init__(self, **kwargs):
        super(IeAmdDigiprov, self).__init__(**kwargs)
        self.root.attrib['ID'] = 'ie-amd-digiprov'

# rep level classes
class RepAmd(Amd):
    def __init__(self, rep_id, **kwargs):
        super(RepAmd, self).__init__(**kwargs)
        self.root.attrib['ID'] = 'rep%s-amd' % (rep_id,)

class RepAmdTech(TechMd):
    def __init__(self, rep_id, **kwargs):
        super(RepAmdTech, self).__init__(**kwargs)
        self.root.attrib['ID'] = 'rep%s-amd-tech' % (rep_id,)

class RepAmdRights(RightsMd):
    def __init__(self, rep_id, **kwargs):
        super(RepAmdRights, self).__init__(**kwargs)
        self.root.attrib['ID'] = 'rep%s-amd-rights' % (rep_id,)

class RepAmdSource(SourceMd):
    def __init__(self, rep_id, **kwargs):
        super(RepAmdSource, self).__init__(**kwargs)
        self.root.attrib['ID'] = 'rep%s-amd-source' % (rep_id,)

class RepAmdDigiprov(DigiprovMd):
    def __init__(self, rep_id, **kwargs):
        super(RepAmdDigiprov, self).__init__(**kwargs)
        self.root.attrib['ID'] = 'rep%s-amd-digiprov' % (rep_id,)

# file level classes
class FileAmd(Amd):
    def __init__(self, file_id, rep_id, **kwargs):
        super(FileAmd, self).__init__(**kwargs)
        self.root.attrib['ID'] = 'fid%s-%s-amd' % (file_id, rep_id,)

class FileAmdTech(TechMd):
    def __init__(self, file_id, rep_id, **kwargs):
        super(FileAmdTech, self).__init__(**kwargs)
        self.root.attrib['ID'] = 'fid%s-%s-amd-tech' % (file_id, rep_id,)

class FileAmdRights(RightsMd):
    def __init__(self, file_id, rep_id, **kwargs):
        super(FileAmdRights, self).__init__(**kwargs)
        self.root.attrib['ID'] = 'fid%s-%s-amd-rights' % (file_id, rep_id,)

class FileAmdSource(SourceMd):
    def __init__(self, file_id, rep_id, **kwargs):
        super(FileAmdSource, self).__init__(**kwargs)
        self.root.attrib['ID'] = 'fid%s-%s-amd-source' % (file_id, rep_id,)

class FileAmdDigiProv(DigiprovMd):
    def __init__(self, file_id, rep_id, **kwargs):
        super(FileAmdDigiProv, self).__init__()
        self.root.attrib['ID'] = 'fid%s-%s-amd-digiprov' % (file_id, rep_id,)

# filesec
class FileSec:
    def __init__(self, flgrp_dict):
        self.root = ET.Element("{%s}fileSec" % METS_NS, nsmap=mets_nsmap)
        for rep in flgrp_dict:
            for item in rep:
                id_val = item
                rep_no = id_val[3:]
                rep_admid = id_val + '-amd'
                self.fileGrp = ET.SubElement(self.root, "{%s}fileGrp" % METS_NS)
                use_type = rep[item][0]['USE']

                self.fileGrp.set('USE', use_type)
                self.fileGrp.set('ID', id_val)
                self.fileGrp.set('ADMID', rep_admid)
                for file_item in rep[item][1]:
                    for fi in file_item:
                        file_id = fi
                        file_admid = file_id + '-' + rep_no + '-amd'
                        # mimeType = file_item[file_id]['MIMETYPE']

                        self.file_element = ET.SubElement(self.fileGrp, "{%s}file" % METS_NS)
                        self.file_element.set('ID', file_id + '-' + rep_no)
                        # self.file_element.set('MIMETYPE', mimeType)
                        self.file_element.set('ADMID', file_admid)

                        href = file_item[file_id]['href']

                        self.FLocat = ET.SubElement(self.file_element, "{%s}FLocat" % METS_NS, nsmap=xlin_nsmap)
                        self.FLocat.set('LOCTYPE', 'URL')
                        self.FLocat.set('{%s}href' % XLIN_NS, href)


def recurse_over_filedict(root_item, input_dict):
    for key in input_dict.keys():
        if type(input_dict[key]) is str:
            fileDiv = ET.SubElement(root_item, "{%s}div" % METS_NS, LABEL=key, TYPE="FILE")
            fptr = ET.SubElement(fileDiv, "{%s}fptr" % METS_NS, FILEID=input_dict[key])
        elif type(input_dict[key]) is OrderedDict:
            folderDiv = ET.SubElement(root_item, "{%s}div" % METS_NS, LABEL=key)
            recurse_over_filedict(folderDiv, input_dict[key])


def populate_file_dict(file_path_list, file_name, file_id, init_dict):
    if len(file_path_list) > 0:
        if file_path_list[-1] in init_dict.keys():
            populate_file_dict(file_path_list[:-1], file_name, file_id, init_dict[file_path_list[-1]])
        else:
            init_dict[file_path_list[-1]] = OrderedDict()
            populate_file_dict(file_path_list[:-1], file_name, file_id, init_dict[file_path_list[-1]])
    else:
        init_dict[file_name] = file_id


class StructMap:
    def __init__(self, repId=None, repType=None, presType=None, fileDict=None):
        '''For building a Structure Map for a representation.

        Args:
            repId (str): representation ID, e.g. '1', '2', etc.
            repType (str): e.g. 'PHYSICAL', 'DIGITAL', etc.
            presType (str): e.g. 'PRESERVATION MASTER', 'MODIFIED MASTER', etc.
            fileDict (list): e.g -
                {"path":
                    {"to":
                        {"file":
                            {
                                "file1.fl": "fid1-1",
                                "file2.fl": "fid2-1"
                            },
                         "different":
                            {"file":
                                {
                                    "file3.fl": "fid3-1"
                                },
                            }
                        }
                    }
                  }
        Returns:
            XML object of Structure Map.
         '''
        self.root = ET.Element("{%s}structMap" % METS_NS, nsmap=mets_nsmap)
        self.presTypeDiv = ET.SubElement(self.root, "{%s}div" % METS_NS)
        self.contentsTableDiv = ET.SubElement(self.presTypeDiv, "{%s}div" % METS_NS)
        self.contentsTableDiv.set('LABEL', 'Table of Contents')
        if repId != None:
            self.root.set('ID', repId)
        if repType != None:
            self.root.set('TYPE', repType)
        if presType != None:
            self.presTypeDiv.set('LABEL', presType)
        if fileDict != None:
            recurse_over_filedict(self.contentsTableDiv, fileDict)


                            
def os_path_split_asunder(path, debug=False):
    parts = []
    while True:
        newpath, tail = os.path.split(path)
        if debug: print(repr(path), (newpath, tail))
        if newpath == path:
            assert not tail
            if path: parts.append(path)
            break
        parts.append(tail)
        path = newpath
    parts.reverse()
    # Added 09/09/2015: hack to remove leading slash - make this better!
    if parts[0] == "/" or parts[0] == "\\":
        parts = parts[1:]
    return parts


def generate_md5(filepath, block_size=2**20):
    """For producing md5 checksums for a file at a specified filepath."""
    m = hashlib.md5()
    with open(filepath, "rb") as f:
        while True:
            buf = f.read(block_size)
            if not buf:
                break
            m.update(buf)
    return m.hexdigest()

def ordered_file_list(rep_directory_path):
    """Checks to see if all files in a directory have integers for filenames,
    and if they do, they will be sorted numerically for the structMap."""
    # NOTE: this function assumes that the files either have no extension,
    # or that they contain a single extension. If, for example, you have a
    # directory full of files like 1.tar.gz, 2.tar.gz, etc, they will not be
    # sorted numerically.

    file_list = os.walk(rep_directory_path)
    output_file_list = []

    for root, dirs, files in file_list:
        int_file_names = []
        str_file_names = []
        for item in files:
            try:
                # coercing to unicode to better handle utf8 characters
                item = unicode(item, "utf-8") 
                item_test = item[:item.rfind(".")]
                int(item_test)
                int_file_names.append(item)
            except ValueError:
                str_file_names.append(item)
        if len(int_file_names) > 0:
            int_file_names = sorted(int_file_names, key=lambda x: int(x[:x.rfind(".")]))
        files = str_file_names + int_file_names
        for item in files:
            output_file_list.append(os.path.join(root,item))
    return output_file_list

def parse_rep_directory(mets, rep_directory_path, pres_type, idNo, digital_original=False):
    """Generates rep-level and file-level METS metadata for a representation
        in a particular directory."""
    if rep_directory_path and len(os.listdir(rep_directory_path)) > 0:
        idNo=idNo
        generalRepCharacteristics=[{'preservationType': pres_type,
                                    'usageType': 'VIEW',
                                    'DigitalOriginal': str(digital_original).lower(),
                                    'RevisionNumber': '1'},]
        rep_amd = RepAmd(rep_id=idNo)
        mets.root.append(rep_amd.root)
        rep_amd.root.append(RepAmdTech(
            rep_id=idNo,
            generalRepCharacteristics=generalRepCharacteristics).root)
        rep_amd.root.append(RepAmdRights(rep_id=idNo).root)
        rep_amd.root.append(RepAmdSource(rep_id=idNo).root)
        rep_amd.root.append(RepAmdDigiprov(rep_id=idNo).root)


        flNo = 0
        file_list = ordered_file_list(rep_directory_path)

        for item in file_list:
            flNo += 1
            filepath = item
            general_file_chars_dict = {}
            # fileMIMEType = mimetypes.guess_type(filepath)[0]
            # if fileMIMEType != None:
            #     general_file_chars_dict['fileMIMEType'] = fileMIMEType
            general_file_chars_dict['fileOriginalName'] = os.path.basename(item)
            general_file_chars_dict['fileOriginalPath'] = item
            general_file_chars_dict['fileSizeBytes'] = str(
                os.path.getsize(filepath))
            general_file_chars_dict['fileModificationDate'] = time.strftime(
                "%Y-%m-%dT%H:%M:%S",time.localtime(os.path.getmtime(filepath)))
            general_file_chars_dict['fileCreationDate'] = time.strftime(
                "%Y-%m-%dT%H:%M:%S",time.localtime(os.path.getctime(filepath)))
            generalFileCharacteristics=[general_file_chars_dict,]
            file_fixity_list = [{"fixityType": "MD5",
                                 "fixityValue": generate_md5(filepath)},]
            fl_amd = FileAmd(file_id=flNo, rep_id=idNo)
            fl_amd.root.append(FileAmdTech(
                file_id=flNo, rep_id=idNo,
                generalFileCharacteristics=generalFileCharacteristics,
                fileFixity=file_fixity_list).root)
            # ft = FileAmdTech(file_id=flNo, rep_id=idNo,
            #                  generalFileCharacteristics=generalFileCharacteristics,
            #                  fileFixity=file_fixity_list)
            fl_amd.root.append(FileAmdRights(file_id=flNo, rep_id=idNo).root)
            fl_amd.root.append(FileAmdSource(file_id=flNo, rep_id=idNo).root)
            fl_amd.root.append(FileAmdDigiProv(file_id=flNo, rep_id=idNo).root)

            mets.root.append(fl_amd.root)

def generate_flgrp_details(mets, rep_directory_path, idNo, pres_type, input_dir):
    """Generates the fileGrp details for a representation in the form of
    a list containing a dictionary for each file in the rep.
    At the same time, a structMap is also generated for the rep and
    appended to the METS."""
    # start off with structMap details
    repId = 'rep' + idNo + '-1'
    repType = 'PHYSICAL'
    presType = pres_type
    fileDict = OrderedDict()
    # presType: 'Preservation Master'
    # fileDict: [ {'Image 1': [{'TYPE': 'FILE'}, {'FILEID': 'fid1-1'}]},
    #             {'Image 2': [{'TYPE': 'FILE'}, {'FILEID': 'fid2-1'}]} ]
    # Now do flgrp details
    fd = {'rep' + idNo: [{"USE": "VIEW"},[],]}
    fileNo = 0
    oo = input_dir
    file_list = ordered_file_list(rep_directory_path)
    for item in file_list:
        fileNo += 1
        filepath = item
        # mime_type = mimetypes.guess_type(filepath)[0]
        # if mime_type == None:
        #     print("A MIME type could not be generated for the file" + filepath)
        #     print("Exiting now.")
        #     exit()
        # the filepath needs to be relative to the SIP structure, not the
        # absolute path on the current machine. So, trim the filepath here!
        filepath = filepath[len(input_dir):]
                # 09/09: Hack to remove the leading slash
        if filepath[0] == "/" or filepath[0] == "\\":
            filepath = filepath[1:]
        file_details = {'fid' + str(fileNo) :
                            {
                            # 'MIMETYPE': mime_type,
                            'href': filepath} }
        fd['rep' + idNo][1].append(file_details)

        # time to build the fileDict for the StructMap!


        item = item[item.find(rep_directory_path)+ len(rep_directory_path): ]

        file_path_dict = os_path_split_asunder(item)
        # grab the filename from file_path_dict
        file_name = file_path_dict.pop()
        # reverse the file_path_dict, so we can easily 
        # pop off the dirs in order
        file_path_dict = file_path_dict[::-1]      
        populate_file_dict(file_path_dict, 
                           file_name, "fid%s-%s" % (str(fileNo), 
                           idNo), 
                           fileDict)
        # Now, back to the structMap!
    mets.root.append(StructMap(repId=repId, repType=repType, presType=presType, fileDict=fileDict).root)
    return fd

def build_rep_amd(rep_id, generalRepCharacteristics=None):
    """Returns a populated repAmd section."""
    rep_id = str(rep_id)
    rep_amd = RepAmd(rep_id=rep_id)
    rep_amd.root.append(RepAmdTech(rep_id=rep_id, 
                        generalRepCharacteristics=generalRepCharacteristics).root)
    rep_amd.root.append(RepAmdRights(rep_id=rep_id).root)
    rep_amd.root.append(RepAmdSource(rep_id=rep_id).root)
    rep_amd.root.append(RepAmdDigiprov(rep_id=rep_id).root)
    return rep_amd

def build_file_amd(file_id, rep_id, generalFileCharacteristics=None,
                    fileFixity=None):
    """Returns a populated fileAmd section."""
    rep_id = str(rep_id)
    file_id = str(file_id)
    file_amd = FileAmd(file_id=file_id, rep_id=rep_id)
    file_amd.root.append(FileAmdTech(file_id=file_id, rep_id=rep_id,
        generalFileCharacteristics=generalFileCharacteristics,
        fileFixity=fileFixity).root)
    file_amd.root.append(FileAmdRights(file_id=file_id, rep_id=rep_id).root)
    file_amd.root.append(FileAmdSource(file_id=file_id, rep_id=rep_id).root)
    file_amd.root.append(FileAmdDigiProv(file_id=file_id, rep_id=rep_id).root)
    return file_amd

# def build_mets(title=None, provenance=None, isPartOf=None, identifier=None,
#                 bibCitation=None, available=None, publisher=None,
#                 pres_master_dir=None, modified_master_dir=None,
#                 access_derivative_dir=None,
#                 cms=None,
#                 generalIECharacteristics=None,
#                 objectIdentifier=None,
#                 accessRightsPolicy=None,
#                 eventList=None,
#                 input_dir=None):
def build_mets(ie_dmd_dict=None,
                pres_master_dir=None, 
                modified_master_dir=None,
                access_derivative_dir=None,
                cms=None,
                webHarvesting=None,
                generalIECharacteristics=None,
                objectIdentifier=None,
                accessRightsPolicy=None,
                eventList=None,
                input_dir=None,
                digital_original=False):
    mets = Mets()
    # mets.root.append(IeDmd(title=title, provenance=provenance, isPartOf=isPartOf,
    #              identifier=identifier, bibCitation=bibCitation, available=available,
    #              publisher=publisher).root)
    mets.root.append(IeDmd(ie_dmd_dict).root)
    ie_amd = IeAmd(webHarvesting=webHarvesting,
        generalIECharacteristics=generalIECharacteristics,
        objectIdentifier=objectIdentifier, cms=cms,
        accessRightsPolicy=accessRightsPolicy, eventList=eventList)
    mets.root.append(ie_amd.root)

    flgrp_dict = []
    if pres_master_dir != None and modified_master_dir != None and access_derivative_dir != None:
        parse_rep_directory(mets, pres_master_dir, 'PRESERVATION_MASTER', "1", digital_original)
        flgrp_dict.append(generate_flgrp_details(mets=mets, rep_directory_path=pres_master_dir, idNo="1", pres_type="Preservation Master", input_dir=input_dir))
        parse_rep_directory(mets, modified_master_dir, 'MODIFIED_MASTER', "2", digital_original)
        flgrp_dict.append(generate_flgrp_details(mets=mets, rep_directory_path=modified_master_dir, idNo="2", pres_type="Modified Master", input_dir=input_dir))
        parse_rep_directory(mets, access_derivative_dir, 'DERIVATIVE_COPY', "3", digital_original)
        flgrp_dict.append(generate_flgrp_details(mets=mets, rep_directory_path=access_derivative_dir, idNo="3", pres_type="Derivative Copy", input_dir=input_dir))

    elif pres_master_dir != None and access_derivative_dir != None:
        parse_rep_directory(mets, pres_master_dir, 'PRESERVATION_MASTER', "1", digital_original)
        flgrp_dict.append(generate_flgrp_details(mets=mets, rep_directory_path=pres_master_dir, idNo="1", pres_type="Preservation Master", input_dir=input_dir))
        parse_rep_directory(mets, access_derivative_dir, 'DERIVATIVE_COPY', "2", digital_original)
        flgrp_dict.append(generate_flgrp_details(mets=mets, rep_directory_path=access_derivative_dir, idNo="2", pres_type="Derivative Copy", input_dir=input_dir))

    elif pres_master_dir != None and modified_master_dir != None:
        parse_rep_directory(mets, pres_master_dir, 'PRESERVATION_MASTER', "1", digital_original)
        # mets.root.append(pres_master_dir, 'PRESERVATION_MASTER', "1")
        flgrp_dict.append(generate_flgrp_details(mets=mets, rep_directory_path=pres_master_dir, idNo="1", pres_type="Preservation Master", input_dir=input_dir))
        parse_rep_directory(mets, modified_master_dir, 'MODIFIED_MASTER', "2", digital_original)
        # mets.root.append(modified_master_dir, 'MODIFIED_MASTER', "2")
        flgrp_dict.append(generate_flgrp_details(mets=mets, rep_directory_path=modified_master_dir, idNo="2", pres_type="Modified Master", input_dir=input_dir))

    elif pres_master_dir != None:
        parse_rep_directory(mets, pres_master_dir, 'PRESERVATION_MASTER', "1", digital_original)
        flgrp_dict.append(generate_flgrp_details(mets=mets, rep_directory_path=pres_master_dir, idNo="1", pres_type="Preservation Master", input_dir=input_dir))

    if len(flgrp_dict) > 0:
        mets.root.append(FileSec(flgrp_dict=flgrp_dict).root)

    # reposition the structmap so it is at the final point of the 
    structmap_list = mets.root.xpath("/mets:mets/mets:structMap", namespaces={'mets': 'http://www.loc.gov/METS/'})
    for structmap in structmap_list:
        mets.root.append(structmap)
        
    return mets

def main():
    parser = argparse.ArgumentParser(description='For creating METS XML file.')

    parser.add_argument('-t', '--title', action="store")
    parser.add_argument('-S', '--ispartof', action="store")
    parser.add_argument('-v', '--provenance', action="store")
    parser.add_argument('-i', '--identifier', action="store")
    parser.add_argument('-n', '--bibcitation', action="store")
    parser.add_argument('-a', '--available', action="store")
    parser.add_argument('-p', '--publisher', action="store")
    parser.add_argument('-I', '--input_dir', action="store")
    parser.add_argument('-P', '--preservation_master_dir', action="store")
    parser.add_argument('-M', '--modified_master_dir', action="store")
    parser.add_argument('-A', '--access_derivative_dir', action="store")
    parser.add_argument('-c', '--cms_id', action="store")
    parser.add_argument('-C', '--cms_system', action="store")
    parser.add_argument('-b', '--object_identifier_value', action="store")
    parser.add_argument('-B', '--object_identifier_type', action="store")
    parser.add_argument('-r', '--access_rights_policy_id', action="store")
    parser.add_argument('-R', '--access_rights_policy_description', action="store")
    parser.add_argument('-T', '--ie_entity_type', action="store")
    parser.add_argument('-s', '--submission_reason', action="store")
    parser.add_argument('-u', '--status', action="store")
    parser.add_argument('-O', '--output_file', action="store")


    args = vars(parser.parse_args())

    if args['cms_id'] and args['cms_system']:
        cms = [{'system': args['cms_system'], 'recordId': args['cms_id']}]
    else:
        cms = None

    general_ie_characteristics = None
    if args['ie_entity_type'] or args['status'] or args['submission_reason']:
        general_ie_characteristics = {}
        if args['ie_entity_type']:
            general_ie_characteristics['IEEntityType'] = args['ie_entity_type']
        if args['status']:
            general_ie_characteristics['status'] = args['status']
        if args['submission_reason']:
            general_ie_characteristics['submissionReason'] = args['submission_reason']
        general_ie_characteristics = [general_ie_characteristics,]

    access_rights = None
    if args['access_rights_policy_description'] or args['access_rights_policy_id']:
        access_rights = {}
        if args['access_rights_policy_id']:
            access_rights['policyId'] = args['access_rights_policy_id']
        if args['access_rights_policy_description']:
            access_rights['policyDescription'] = args['access_rights_policy_description']
        access_rights = [access_rights,]

    object_identifier = None
    if args['object_identifier_type'] and args['object_identifier_value']:
        object_identifier = {'objectIdentifierType': args['object_identifier_type'],
                             'objectIdentifierValue': args['object_identifier_value']}
        object_identifier = [object_identifier,]

    event_list = [{'eventIdentifierType': 'EXTERNAL',
                  'eventIdentifierValue': 'EXT_1',
                  'eventType': 'METS_FILE_GENERATED',
                  'eventDescription': 'METS file created using the METSify tool version %s' % __version__,
                  'eventDateTime': datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
                  }]

    ie_dmd_dict = {}
    if args['title']:
        ie_dmd_dict["dc:title"] = args['title']
    if args['provenance']:
        ie_dmd_dict["dcterms:provenance"] = args['provenance']
    if args['isPartOf']:
        ie_dmd_dict["dcterms:isPartOf"] = args['isPartOf']
    if args['identifier']:
        ie_dmd_dict["dc:identifier xsi:type=ArchwayUniqueId"] = args['identifier']
    if args['bibCitation']:
        ie_dmd_dict["dcterms:bibliographicCitation"] = args['bibCitation']
    if args['available']:
        ie_dmd_dict["dcterms:available"] = args['available']
    if args['publisher']:
        ie_dmd_dict["dcterms:publisher"] = args['publisher']
    
    mets = build_mets(ie_dmd_dict=ie_dmd_dict,
                      pres_master_dir=args['preservation_master_dir'],
                      modified_master_dir=args['modified_master_dir'],
                      access_derivative_dir=args['access_derivative_dir'],
                      cms=cms,
                      generalIECharacteristics=general_ie_characteristics,
                      objectIdentifier=object_identifier,
                      accessRightsPolicy=access_rights,
                      eventList = event_list,
                      input_dir=args['input_dir'])

    if args['output_file']:
        output_filename = args['output_file']
        with open(output_filename, "wb") as xml:
            xml.write(ET.tostring(mets.root, pretty_print=True))
            xml.close()
    else:
        print(ET.tostring(mets.root))
if __name__ == '__main__':
    exit(main())
