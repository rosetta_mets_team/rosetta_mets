#rosetta_mets
Generate Rosetta-compliant METS XML files.

##Background
The Rosetta digital preservation application provides multiple different avenues for depositing digital content, including a web interface deposit, csv and METS XML. The METS XML process lends itself well to processing large numbers of records, and Ex Libris (the developers of Rosetta) provide a Java-based SDK for constructing deposit mechanisms (available at https://github.com/ExLibrisGroup/Rosetta.dps-sdk-projects).  
This tool provides functionality for creating METS XML based deposits for the Rosetta application, but it is built with Python rather than Java. While it does not offer the full range of options that are available with the Java-based deposit SDK, it aims to provide a quick and easy mechanism for common use cases, and aims to appeal to people who prefer working with Python rather than Java.

##Usage
To use as part of a different program:
```python
import mets

mets_file = mets.build_mets(
    ie_dmd_dict=None,
    pres_master_dir=None,
    modified_master_dir=None,
    access_derivative_dir=None,
    cms=None,
    generalIECharacteristics=None,
    objectIdentifier=None,
    accessRightsPolicy=None,
    eventList=None,
    input_dir=None
    digital_original=False
)
```

The above arguments (if required) should be submitted in the following format:

ie_dmd_dict = dictionary, such as follows:
```python
{"dc:title": "Title of record", "dcterms:isPartOf": "19926",
 "dc:identifier xsi:type=InstitionalIdentifier": "A1234",
 "dcterms:provenance": "Transferred from Agency ABCD"}
```
(See below in the "dc, dcterms and xsi mapping in ie_dmd" description for more
details about building an ie_dmd section)  
pres_master_dir = string  
modified_master_dir = string  
access derivative_dir = string  
cms = dictionary inside list, such as follows:  
```python
[{'system': <system name>, 'recordId': <CMS ID> },]
```  
generalIECharacteristics = dictionary inside list, such as follows:  
```python
[{'IEEntityType': <entity type>, 'submissionReason': <submission reason>},]
```  
objectIdentifier = dictionary inside list, such as follows:  
```python
[{'policyId': <policy ID>, 'policyDescription': <policy description>},]
```  
eventList = dictionary inside list, such as follows:  
```python
[{'eventDateTime': <event datetime>, 
  'eventType': <event type>,
  'eventIdentifierType': <event identifier type>, 
  'eventIdentifierValue': <event identifier value>,
  'eventOutcome1': <event outcome 1>,
  'eventOutcomeDetail1': <event outcome detail 1>,
  'eventDescription': <event description>,
  'linkingAgentIdentifierType1': <linking agent identifier type 1>,
  'linkingAgentIdentifierValue1': <linking agent identifier value 1>
  },]
```  
(**Note**: Not all key/value pairs are required for events.)  
input_dir = string  

digital_original = Boolean (default is False)  
   
###add file-level event   
To add a file-level event to a mets object:   
```python
mets.add_file_level_event(file_location, event_dictionary)
```   
where `file_location` is equal to the filepath of the file in question,
starting from the base SIP location, such as 
`"sip1/pm/image001.jpg"`.
`event_dictionary` is like the eventList used when building the METS object,
but it is not inside a list. You can still add as many events as you like,
but each event must be built with a seperate call to the add_file_level_event
method.   
An example event_dictionary could look like this:   
```python
{'eventDateTime': '2015-08-31 11:06:17',
 'eventIdentifierType': 'EXTERNAL',
 'eventIdentifierValue': EXT_1,
 'eventType': 'FILE MODIFICATION',
 'eventDescription': 'changed byte at offset 08 to 0xf6 from 0xa6'}
```
   
   
To write the XML object to a file, the METS object has a write_to_file() method. It is
called like so:   
`mets.write_to_file('path/to/file.xml')`   
By default, pretty_print is switched on. If you want it turned off, call the method like
this:   
`mets.write_to_file('path/to/file.xml', pretty_print=False)`   

To print the METS object to stdout:   
`mets.print_mets()`   
`mets.print_mets(pretty_print=False)`   

To store the METS object as a string:   
`mets.as_string()`   
`mets.as_string(pretty_print=False)`   
   
For details on using this module as a standalone script, enter the command:  
`mets --help`.  

###Typical SIP folder structure
The typical folder structure for a SIP as as follows:
```
Base_location_on_server
|
|_sip_folder
    |
    |_content
         |
         |_settings
         |   |
         |   |_settings.properties
         |
         |_streams
         |   |
         |   |_file1.txt
         |   |
         |   |_file2.txt
         |
         |_mets.xml
```
alternatively, if you want to have different representations in different subfolders, you can order it like so:   
```
Base_location_on_server
|
|_sip_folder
    |
    |_content
         |
         |_settings
         |   |
         |   |_settings.properties
         |
         |_streams
         |   |
         |   |_pres_master
         |   |    |
         |   |    |_file1.tif
         |   |    |
         |   |    |_file2.tif
         |   |
         |   |_modified_master
         |   |    |
         |   |    |_file1.jpg
         |   |    |
         |   |    |_file2.jpg
         |   |
         |   |_access_derivative
         |        |
         |        |_file1.pdf
         |    
         |_mets.xml
```


You can have as many “sip_folder” folders in the “Base_location_on_server” as you like.  
Depending on your mode of deposit, the settings folder and settings.properties file is not required. For instance, it is not required if you are initiating the deposit via the submitDepositAcitivity call of the Rosetta deposit web services.   
The “settings.properties” file looks like this:

```
material_flow_id=12345
deposit_set_id=1
user_name=username
user_password=password
user_institution=INS00
user_producer_id=99999
```
with your appropriate values instead of the placeholder values.

###dc, dcterms and xsi mapping in ie_dmd
The ie_dmd component does some behind-the scenes parsing of namespace prefixes 
and attributes. Specifically, the following three namespaces are supported:   
dc - is mapped to "http://purl.org/dc/elements/1.1/"   
dcterms - is mapped to "http://purl.org/dc/terms/"   
xsi - is mapped to "http://www.w3.org/2001/XMLSchema-instance", and is
      intended only for use with attributes, not the element names.

##Installing rosetta_mets
If you have downloaded the package, unzip it and execute the following command:  
`python setup.py install`  
This package will also install the most recent lxml library via pip if it is
not already installed.