import unittest
import mets as mfc
from lxml import etree as ET

SIP_1_LOCATION = 'test/input_files/sip_1/'
SIP_2_LOCATION = 'test/input_files/sip_2/'
SIP_OUTPUT_LOCATION = 'test/output_files/'

SIP_1_METS = SIP_OUTPUT_LOCATION + "sip_1.xml"

generalIECharacteristics = [{"IEEntityType": "ANZ_DigitisedText",
                        "submissionReason": "Digitised"}]
objectIdentifier = [{'objectIdentifierType': 'ArchwayUniqueID',
                 'objectIdentifierValue': 'R22271462'}]
cms = None
accessRightsPolicy = [{'policyId': '1916130',
             'policyDescription': 'Open access'}]
eventList = None


sip_1_ie_dict = {"dc:title": "Test METS creation Deposit",
                 "dc:identifier xsi:type=ArchwayUniqueId": "R22271462",
                 "dcterms:isPartOf": "18805",
                 "dcterms:provenance": "Transferred by agency AABK"}

sip_1_mets = mfc.build_mets(ie_dmd_dict=sip_1_ie_dict,
                              pres_master_dir=SIP_1_LOCATION + "pm/", 
                              modified_master_dir=None,
                              access_derivative_dir=SIP_1_LOCATION + "ad/",
                              cms=None,
                              generalIECharacteristics=generalIECharacteristics,
                              objectIdentifier=objectIdentifier,
                              accessRightsPolicy=accessRightsPolicy,
                              eventList=None,
                              input_dir=SIP_1_LOCATION)
sip_2_ie_dict = {"dc:title": "Test METS creation Deposit",
                 "dc:identifier xsi:type=ArchwayUniqueId": "R22271462",
                 "dcterms:isPartOf": "18805",
                 "dcterms:provenance": "Transferred by Agency AABK"}
sip_2_mets = mfc.build_mets(ie_dmd_dict=sip_2_ie_dict,
                              pres_master_dir=SIP_2_LOCATION, 
                              modified_master_dir=None,
                              access_derivative_dir=None,
                              cms=None,
                              generalIECharacteristics=generalIECharacteristics,
                              objectIdentifier=objectIdentifier,
                              accessRightsPolicy=accessRightsPolicy,
                              eventList=None,
                              input_dir=SIP_2_LOCATION)



sample_ie_amd_string = '<mets:amdSec ID="ie-amd" xmlns:mets="http://www.loc.gov/METS/">\
            <mets:techMD ID="ie-amd-tech">\
                <mets:mdWrap MDTYPE="OTHER" OTHERMDTYPE="dnx">\
                    <mets:xmlData>\
                        <dnx xmlns:dnx="http://www.exlibrisgroup.com/dps/dnx">\
                        <section id="generalIECharacteristics">\
                            <record>\
                                <key id="submissionReason">Digitised</key>\
                                <key id="IEEntityType">ANZ_DigitisedText</key>\
                            </record>\
                        </section>\
                        <section id="objectIdentifier">\
                            <record>\
                                <key id="objectIdentifierType">ArchwayUniqueID</key>\
                                <key id="objectIdentifierValue">R22271462</key>\
                            </record>\
                        </section>\
                        </dnx>\
                    </mets:xmlData>\
                </mets:mdWrap>\
            </mets:techMD>\
            <mets:rightsMD ID="ie-amd-rights">\
                <mets:mdWrap MDTYPE="OTHER" OTHERMDTYPE="dnx">\
                    <mets:xmlData>\
                        <dnx xmlns:dnx="http://www.exlibrisgroup.com/dps/dnx">\
                        <section id="accessRightsPolicy">\
                            <record>\
                                <key id="policyId">1916130</key>\
                                <key id="policyDescription">Open access</key>\
                            </record>\
                        </section>\
                        </dnx>\
                    </mets:xmlData>\
                </mets:mdWrap>\
            </mets:rightsMD>\
            <mets:rightsMD ID="ie-amd-source">\
                <mets:mdWrap MDTYPE="OTHER" OTHERMDTYPE="dnx">\
                    <mets:xmlData>\
                        <dnx xmlns:dnx="http://www.exlibrisgroup.com/dps/dnx"/>\
                    </mets:xmlData>\
                </mets:mdWrap>\
            </mets:rightsMD>\
            <mets:rightsMD ID="ie-amd-digiprov">\
                <mets:mdWrap MDTYPE="OTHER" OTHERMDTYPE="dnx">\
                    <mets:xmlData>\
                        <dnx xmlns:dnx="http://www.exlibrisgroup.com/dps/dnx"/>\
                    </mets:xmlData>\
                </mets:mdWrap>\
            </mets:rightsMD>\
        </mets:amdSec>'

def create_anz_ie_dmd(title,provenance,isPartOf,identifier):
    ie_dmd_dict = {"dc:title": title,
                   "dcterms:provenance": provenance,
                   "dcterms:isPartOf": isPartOf,
                   "dc:identifier xsi:type=ArchwayUniqueID": identifier}
    mets_ie_dmd = mfc.IeDmd(ie_dmd_dict=ie_dmd_dict)
    return mets_ie_dmd


class MetsTest(unittest.TestCase):
    """Testing the components of the METS generation process."""

    def test_anz_ie_dmd_sec(self):
        self.sample_dmd_string = '<mets:dmdSec xmlns:mets="http://www.loc.gov/METS/" ID="ie-dmd"><mets:mdWrap MDTYPE="DC"><mets:xmlData><mets:record xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:dcterms="http://purl.org/dc/terms/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><dc:title>title of record</dc:title><dcterms:provenance>transferred from agency AABC</dcterms:provenance><dc:identifier xsi:type="archwayUniqueId">R121212</dc:identifier><dcterms:isPartOf>2002</dcterms:isPartOf></mets:record></mets:xmlData></mets:mdWrap></mets:dmdSec>'
        self.parsed_xml = ET.fromstring(self.sample_dmd_string)

        title = "title of record"
        provenance = "transferred from agency AABC"
        isPartOf = "2002"
        identifier = "R121212"

        self.dmd = create_anz_ie_dmd(title=title, isPartOf=isPartOf,
                                     provenance=provenance,
                                     identifier=identifier)
        dmd_root = self.dmd.root
        parsed_xml_root = self.parsed_xml
        self.assertEqual(dmd_root.tag, parsed_xml_root.tag)
        self.assertEqual(dmd_root[0].tag, parsed_xml_root[0].tag)

        dmd_root_values = dmd_root[0][0][0]
        parsed_xml_root_values = parsed_xml_root[0][0][0]

        for element in dmd_root_values:
            if element.text == provenance:
                dmd_prov = element.text

        for element in parsed_xml_root_values:
            if element.text == provenance:
                parsed_prov = element.text

        self.assertEqual(dmd_prov, parsed_prov)


    def test_ie_amd_sec(self):

        self.sample_ie_amd_string = sample_ie_amd_string
        generalIECharacteristics = [{"IEEntityType": "ANZ_DigitisedText",
                                "submissionReason": "Digitised"}]
        objectIdentifier = [{'objectIdentifierType': 'ArchwayUniqueID',
                         'objectIdentifierValue': 'R22271462'}]
        cms = None
        accessRightsPolicy = [{'policyId': '1916130',
                     'policyDescription': 'Open access'}]
        eventList = None

        self.ie_amd = mfc.IeAmd(
            generalIECharacteristics=generalIECharacteristics,
            objectIdentifier=objectIdentifier,
            cms=cms,
            accessRightsPolicy=accessRightsPolicy,
            eventList=eventList)

        self.parsed_ie_amd = ET.fromstring(self.sample_ie_amd_string)

        self.assertEqual(self.ie_amd.root.tag, self.parsed_ie_amd.tag)

        for c1 in self.ie_amd.root:
            for c2 in c1:
                for c3 in c2:
                    for c4 in c3:
                        for c5 in c4:
                            for c6 in c5:
                                for c7 in c6:
                                    if c7.attrib["id"] == "submissionReason":
                                        self.assertEqual(c7.text, "Digitised")
                                    elif c7.attrib["id"] == "objectIdentifierValue":
                                        self.assertEqual(c7.text, "R22271462")

    # After updates to build_mets, this test is no longer valid.
    # The update that broke the test was to reorder the structmap elements
    # so that they sit at the bottom of the tree. The previous structure
    # produced invalid mets files according to the Rosetta XSD.
    # def test_sip_1_results(self):
    #     new_mets = sip_1_mets

    #     test_mets = ET.parse(SIP_OUTPUT_LOCATION + "sip_1.xml")
    #     tm_root = test_mets.getroot()
    #     # print(tm_root.tag)
    #     # print(new_mets.root.tag)
    #     self.assertEqual(tm_root.tag, new_mets.root.tag)
    #     # check that the first-level tags are all the same
    #     tm_children_list = []
    #     tm_struct_map_list = []
    #     new_mets_children_list = []
    #     new_mets_struct_map_list = []
    #     for element in tm_root:
    #         tm_children_list.append(element.tag)
    #         if element.tag == '{http://www.loc.gov/METS/}structMap':
    #             tm_struct_map_list.append(element)
    #     for element in new_mets.root:
    #         new_mets_children_list.append(element.tag)
    #         if element.tag == '{http://www.loc.gov/METS/}structMap':
    #             new_mets_struct_map_list.append(element)

    #     self.assertEqual(len(tm_children_list), len(new_mets_children_list))
    #     for i in range(len(tm_children_list)):
    #         self.assertEqual(tm_children_list[i-1], new_mets_children_list[i-1])

    #     # check the structmaps!
    #     # print(new_mets_struct_map_list[0][0][0][0].attrib)
    #     self.assertEqual(tm_struct_map_list[0][0][0][0].attrib,
    #         new_mets_struct_map_list[0][0][0][0].attrib)

        # pass

    def test_check_sip1_filesec(self):
        for element in sip_1_mets.root:
            if element.tag == '{http://www.loc.gov/METS/}fileSec':
                filesec = element
        self.assertEqual(filesec[0][0][0].attrib['{http://www.w3.org/1999/xlink}href'],
            'pm/1.jpg')
        self.assertEqual(filesec[0][1][0].attrib['{http://www.w3.org/1999/xlink}href'],
            'pm/2.jpg')
        self.assertEqual(filesec[0][2][0].attrib['{http://www.w3.org/1999/xlink}href'],
            'pm/3.jpg')
        self.assertEqual(filesec[0][3][0].attrib['{http://www.w3.org/1999/xlink}href'],
            'pm/4.jpeg')
        self.assertEqual(filesec[0][4][0].attrib['{http://www.w3.org/1999/xlink}href'],
            'pm/5.jpg')
        self.assertEqual(filesec[1][0][0].attrib['{http://www.w3.org/1999/xlink}href'],
            'ad/collate.pdf')

    
    def test_sip_2_results(self):
        pass

    def test_check_sip2_filesec(self):
        for element in sip_1_mets.root:
            if element.tag == '{http://www.loc.gov/METS/}fileSec':
                filesec = element

    def test_add_file_level_event(self):
        file_location = "test/input_files/sip_1/pm/2.jpg"
        test_description = "Migrated from some other place"
        sip_1_mets.add_file_level_event(file_location, 
                {"eventType": "MIGRATION",
                 "eventIdentifierValue": "EXT_1",
                 "eventIdentifierType": "EXTERNAL",
                 "eventDescription": test_description})
        amdsec = sip_1_mets.root.xpath("//mets:amdSec", namespaces={"mets": "http://www.loc.gov/METS/"})
        for file_amdsec in amdsec:
            if file_amdsec.xpath("./*/*/*/*/*/*/key[@id='fileOriginalName']", namespaces={"mets": "http://www.loc.gov/METS/"}):
                if file_amdsec.xpath("./*/*/*/*/*/*/key[@id='fileOriginalName']", namespaces={"mets": "http://www.loc.gov/METS/"})[0].text == file_location:
                    event_description = file_amdsec.xpath("./mets:digiprovMD/mets:mdWrap/mets:xmlData/dnx/section[@id='event']/record/key[@id='eventDescription']", namespaces={"mets": "http://www.loc.gov/METS/"})[0].text
                    self.assertEqual(event_description, test_description)


    def test_create_mets_without_objectIdentifier(self):
        raised = False
        try:
            mfc.build_mets(ie_dmd_dict=sip_1_ie_dict,
                                pres_master_dir=SIP_1_LOCATION + "pm/", 
                                modified_master_dir=None,
                                access_derivative_dir=SIP_1_LOCATION + "ad/",
                                cms=None,
                                generalIECharacteristics=generalIECharacteristics,
                                accessRightsPolicy=accessRightsPolicy,
                                eventList=None,
                                input_dir=SIP_1_LOCATION)
        except:
            raised = True
        self.assertFalse(raised)


    def test_build_mets_with_digital_original_set_to_true(self):
        mets = mfc.build_mets(ie_dmd_dict=sip_1_ie_dict,
                                pres_master_dir=SIP_1_LOCATION + "pm/", 
                                modified_master_dir=None,
                                access_derivative_dir=SIP_1_LOCATION + "ad/",
                                cms=None,
                                generalIECharacteristics=generalIECharacteristics,
                                accessRightsPolicy=accessRightsPolicy,
                                eventList=None,
                                input_dir=SIP_1_LOCATION,
                                digital_original=True)
        digital_original_tags = mets.root.xpath('//section[@id="generalRepCharacteristics"]/record/key[@id="DigitalOriginal"]')
        # print(digital_original_tags[0].text)
        self.assertEqual(digital_original_tags[0].text, 'true')
        self.assertNotEqual(digital_original_tags[1].text, 'false')

    def test_build_mets_with_digital_original_set_to_false(self):
        mets = mfc.build_mets(ie_dmd_dict=sip_1_ie_dict,
                                pres_master_dir=SIP_1_LOCATION + "pm/", 
                                modified_master_dir=None,
                                access_derivative_dir=SIP_1_LOCATION + "ad/",
                                cms=None,
                                generalIECharacteristics=generalIECharacteristics,
                                accessRightsPolicy=accessRightsPolicy,
                                eventList=None,
                                input_dir=SIP_1_LOCATION,
                                digital_original=False)
        digital_original_tags = mets.root.xpath('//section[@id="generalRepCharacteristics"]/record/key[@id="DigitalOriginal"]')
        # print(digital_original_tags[0].text)
        self.assertEqual(digital_original_tags[0].text, 'false')
        self.assertNotEqual(digital_original_tags[1].text, 'true')


    def test_build_repamd_using_convenience_function(self):
        rep_amd = mfc.build_rep_amd(rep_id=1, 
            generalRepCharacteristics=[{'preservationType': 'PRESERVATION_MASTER',
                                   'usageType': 'VIEW',
                                   'DigitalOriginal': 'true',
                                   'RevisionNumber': '1'}])
        # print(ET.tostring(rep_amd.root))
        pres_type = rep_amd.root.xpath('//section[@id="generalRepCharacteristics"]/record/key[@id="preservationType"]')
        self.assertEqual(pres_type[0].text, 'PRESERVATION_MASTER')
        self.assertNotEqual(pres_type[0].text, 'MODIFIED_MASTER')

    def test_build_fileamd_using_convenience_function(self):
        file_amd = mfc.build_file_amd(rep_id=1, file_id=1,
            generalFileCharacteristics=[{'fileCreationDate': '2015-06-13T21:56:57',
                                    'fileModificationDate': '2015-06-21T19:22:08',
                                    'fileOriginalName': 'test_file.txt',
                                    'fileOriginalPath': 'path/to/test_file.txt'}],
            fileFixity=[{'fixityType': 'MD5', 'fixityValue': '0762a8b53fefb7fd54de6c847c83e001'}])
        file_original_name = file_amd.root.xpath('//section[@id="generalFileCharacteristics"]/record/key[@id="fileOriginalName"]')
        self.assertEqual(file_original_name[0].text, 'test_file.txt')
        self.assertNotEqual(file_original_name[0].text, 'not_test_file.txt')

    

if __name__ == '__main__':
    unittest.main()

